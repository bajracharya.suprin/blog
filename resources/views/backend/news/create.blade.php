@extends('layouts.backend_master')

@section('content')
    <section class="content-header">
        <h1>
            Add News
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="#">News</a></li>
            <li class="active">Create</li>
        </ol>
    </section>
    <section class="content">
        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">News List</h3>
                <div class="box-tools pull-right">
                    <a href="{{route('news.index')}}" class="btn btn-default">View All News</a>
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fa fa-times"></i></button>
                </div>
            </div>
            <div class="box-body">
                @if(count($errors) > 0)
                    @foreach($errors as $error)
                        <div class="alert alert-danger">
                            {{error}}
                        </div>
                    @endforeach
                @endif
                <form action="{{route('news.store')}}" method="post">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="name">Title</label>
                        <input type="text" name="title" placeholder="Enter title" class="form-control" id="title">
                        @if($errors->has('title'))
                                <p style="color:red;">
                                    {{$errors->first('title')}}
                                </p>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="name">Slug</label>
                        <input type="text" name="slug" placeholder="Enter slug" class="form-control" id="slug">
                    </div>
                    <div class="form-group">
                        <label for="name">Short Description</label>
                        <input type="text" name="short_description"  class="form-control" id="short_description">
                    </div>
                    <div class="form-group">
                        <label for="name">Description</label>
                        <input type="text" name="description"  class="form-control" id="description">
                    </div>
                    <div class="form-group">
                        <label for="name">Status</label>
                        <input type="radio" name="status" value="1" id="status">Active
                        <input type="radio" name="status" value="0" id="status">Deactive
                         @if($errors->has('slug'))
                                <p style="color:red;">
                                    {{$errors->first('slug')}}
                                </p>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="name">Image</label>
                        <input type="text" name="image" class="form-control" id="image">
                    </div>
                    <div class="form-group">
                        <input type="submit" name="submit"  class="btn btn-success" value="Save">
                    </div>

                </form>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                Footer
            </div>
            <!-- /.box-footer-->
        </div>
        <!-- /.box -->

    </section>
@endsection