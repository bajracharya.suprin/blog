@extends('layouts.backend_master')

@section('content')
    <section class="content-header">
        <h1>
            Update News
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="#">News</a></li>
            <li class="active">Update</li>
        </ol>
    </section>
    <section class="content">
        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">News Form</h3>

                <div class="box-tools pull-right">
                    <a href="{{route('news.index')}}" class="btn btn-default">View All News</a>
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fa fa-times"></i></button>
                </div>
            </div>
            <div class="box-body">
                <form action="{{route('news.edit',$news->id)}}" method="post">
                    <input type="hidden" name="_method" value="put">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="name">Title</label>
                        <input type="text" name="title" placeholder="Enter title" value="{{$news->title}}" class="form-control" id="title">
                    </div>
                    <div class="form-group">
                        <label for="name">Slug</label>
                        <input type="text" name="slug" placeholder="Enter slug" value="{{$news->slug}}"  class="form-control" id="slug">
                    </div>
                    <div class="form-group">
                        <label for="name">Short Description</label>
                        <input type="text" name="short_description" value="{{$news->short_description}}" class="form-control" id="short_description">
                    </div>
                    <div class="form-group">
                        <label for="name">Description</label>
                        <input type="text" name="description"  value="{{$news->description}}" class="form-control" id="description">
                    </div>
                    <div class="form-group">
                        <label for="name">Status</label>
                        <?php if(($news->status) == 1){?>
                        <input type="radio" name="status" value="1" id="status" checked>Active
                        <input type="radio" name="status" value="0" id="status">Deactive
                        <?php }else{ ?>
                        <input type="radio" name="status" value="1" id="status">Active
                        <input type="radio" name="status" value="0" id="status" checked>Deactive
                        <?php } ?>
                    </div>
                    <div class="form-group">
                        <label for="name">Image</label>
                        <input type="text" name="image" class="form-control" id="image">
                    </div>
                    <div class="form-group">
                        <input type="submit" name="submit"  class="btn btn-primary" value="Update">
                    </div>

                </form>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                Footer
            </div>
            <!-- /.box-footer-->
        </div>
        <!-- /.box -->

    </section>
@endsection