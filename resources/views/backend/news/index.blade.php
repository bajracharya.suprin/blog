@extends('layouts.backend_master')

@section('content')
    <section class="content-header">
        <h1>
            News Index
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="#">News</a></li>
            <li class="active">Index</li>
        </ol>
    </section>
    <section class="content">
        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">News List</h3>

                <div class="box-tools pull-right">
                    <a href="{{route('news.create')}}" class="btn btn-default">Add News</a>
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fa fa-times"></i></button>
                </div>
            </div>
            <div class="box-body">
                @if(session('success'))
                    <div class="alert alert-success">
                    {{session('success')}}
                    </div>
                @endif
                @if(session('error'))
                    <div class="alert alert-danger">
                    {{session('error')}}
                    </div>
                @endif
                <table class="table table-bordered table-responsive">
                    <thead>
                        <th>Sn</th>
                        <th>Title</th>
                        <th>Status</th>
                        <th>Image</th>
                        <th>Created Date</th>
                        <th>Action</th>
                    </thead>
                    <?php $i=1; ?>
                    @foreach($news as $n)
                        <tbody>
                            <td>{{$i++}}</td>
                            <td>{{$n->title}}</td>
                            <td><?php if($n->status == 1){ ?>
                                {{'Active'}}
                                <?php }else{ ?> 
                                {{'Inactive'}}
                                 <?php } ?>
                            </td>
                            <td>{{$n->image}}</td>
                            <td>{{$n->created_at}}</td>
                            <td >
                                <p>
                                <a href="{{route('news.update',$n->id)}}" class="btn btn-primary">Update</a>
                                <form action="{{route('news.destroy',$n->id)}}" method="post">
                                    {{csrf_field()}}
                                <input type="hidden" value="delete" name="_method">
                                <input type="submit" value="Delete" class="btn btn-danger">
                                </form>
                                </p>
                            </td>
                        </tbody>
                    @endforeach

                </table>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                {{$news->links()}}
            </div>
            <!-- /.box-footer-->
        </div>
        <!-- /.box -->

    </section>
@endsection