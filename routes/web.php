<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/backend/news', 'NewsController@index')->name('news.index');

Route::post('/backend/news', 'NewsController@store')->name('news.store');

Route::get('/backend/news/create', 'NewsController@create')->name('news.create');

Route::get('/backend/news/update/{id}', 'NewsController@update')->name('news.update');

Route::put('/backend/news/{id}', 'NewsController@edit')->name('news.edit');

Route::delete('/backend/news/{id}', 'NewsController@destroy')->name('news.destroy');