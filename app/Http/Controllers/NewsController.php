<?php

namespace App\Http\Controllers;

use App\News;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\Http\Requests\AddNewsRequest;

class NewsController extends Controller
{
    function index(){
        $news = News::orderby('created_at','desc')->paginate(1);
        return view('backend.news.index',compact('news'));
    }
    function create(){
        return view('backend.news.create');
    }
    function store(AddNewsRequest $request){
        
        $news_status= News::create([
            'title' => $request->input('title'),
            'slug' => $request->input('slug'),
            'short_description' => $request->input('short_description'),
            'description' => $request->input('description'),
            'status' => $request->input('status'),
            'image' => $request->input('image'),
            'created_by' => Auth::user()->id,
        ]);
        if($news_status){
            Session::flash('success','News added successfully.');
        }else{
            Session::flash('error','News not added.');
        }
        return redirect('backend/news');
    }
    function edit(Request $request, $id){
        $news = News::find($id);
        $news->title = $request->input('title');
        $news->slug = $request->input('slug');
        $news->short_description = $request->input('short_description');
        $news->description = $request->input('description');
        $news->status = $request->input('status');
        $news->image = $request->input('image');
        $news->updated_by = Auth::user()->id;
        $news->save();
        return redirect('backend/news');
    }
    function update($id){
        $news = News::find($id);
//        echo '<pre>';
//        print_r($news);die;
        return view('backend.news.update',compact('news'));
    }
     function destroy($id){
        $news = News::find($id);
        $news->delete();
        return redirect('backend/news');
    }
}
