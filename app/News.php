<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $table= 'news';

    protected $fillable = ['title','slug','short_description','description','status','created_by'];
}
